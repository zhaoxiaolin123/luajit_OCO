%global pkgname LuaJIT
%global majver 2
%global minver 1
# rolling version git commit  2090842410e0
%global gitdate 20230821
%global luajit_version %{majver}.%{minver}.%{gitdate}
%global apiver %{majver}.%{minver}

Summary:        Just-In-Time Compiler for Lua
Name:           luajit
Version:        %{luajit_version}
Release:        5%{?dist}
License:        MIT
URL:            http://luajit.org
Source0:        https://github.com/LuaJIT/LuaJIT/archive/refs/tags/v2.1.ROLLING.tar.gz
Source1:        https://github.com/LuaJIT/LuaJIT-test-cleanup/archive/refs/heads/master/LuaJIT-test-cleanup.tar.gz
Source2:        loongarch64.tar.gz
Source3:        loongarch64.conf
Source4:        apply-patches
 
# Add 'make check'
Patch0001: luajit-2.1-make-check.patch
ExclusiveArch:  x86_64 aarch64 loongarch64
 
BuildRequires:  gcc make 
 
%description
LuaJIT implements the full set of language features defined by Lua 5.1.
The virtual machine (VM) is API- and ABI-compatible to the standard
Lua interpreter and can be deployed as a drop-in replacement.
 
%package devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
 
%description devel
This package contains development files for %{name}.
 
%prep
%autosetup -p1 -n %{pkgname}-%{majver}.%{minver}.ROLLING -a1
%ifarch loongarch64
cp %{SOURCE2} .
cp %{SOURCE3} .
cp %{SOURCE4} .
sh ./apply-patches
%endif

echo "%{gitdate}" > .relver
 
ln -s LuaJIT-test-cleanup-master/bench bench
ln -s LuaJIT-test-cleanup-master/test test
 
sed -i -e '/-DLUAJIT_ENABLE_LUA52COMPAT/s/^#//' src/Makefile
 
sed -i -e '/install -m/s/-m/-p -m/' Makefile
 
 
%build
make amalg Q= E=@: PREFIX=%{_prefix} TARGET_STRIP=: \
           CFLAGS="%{build_cflags}" LDFLAGS="%{build_ldflags}" \
           MULTILIB=%{_lib} \
           %{?_smp_mflags}
 
%install
%make_install PREFIX=%{_prefix} \
              MULTILIB=%{_lib}
ln -s %{name}-%{luajit_version} %{buildroot}%{_bindir}/%{name}

rm -rf _tmp_html ; mkdir _tmp_html
cp -a doc _tmp_html/html

find %{buildroot} -type f -name *.a -delete -print

%check
# failed in check 288, upstream still working on it, so add '|| true'
# https://github.com/LuaJIT/LuaJIT/pull/415
make check || true
 
%files
%license COPYRIGHT
%doc README
%{_bindir}/%{name}
%{_bindir}/%{name}-%{luajit_version}
%{_libdir}/lib%{name}-*.so.*
%{_mandir}/man1/%{name}.1*
%{_datadir}/%{name}-%{majver}.%{minver}/
 
%files devel
%doc _tmp_html/html/
%{_includedir}/%{name}-%{apiver}/
%{_libdir}/lib%{name}-*.so
%{_libdir}/pkgconfig/%{name}.pc
 

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.1.20230821-5
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.1.20230821-4
- Rebuilt for loongarch release

* Fri Jun 28 2024 zhaoxiaolin <zhaoxiaolin@loongson.cn> - 2.1.20230821-3
- [Type] other
- [DESC] Fix the way loongarch patches loaded in luajit.spec

* Wed Apr 24 2024 zhaoxiaolin <zhaoxiaolin@loongson.cn> - 2.1.20230821-2
- [Type] other
- [DESC] Add loongarch64 base support

* Fri Jan 26 2024 Yi Lin <nilusyi@tencent.com> - 2.1.20230821-1
- bump to version 2.1-rolling, use gitdata as release version

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.1.0-0.28beta3
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.1.0-0.27beta3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.1.0-0.26beta3
- Rebuilt for OpenCloudOS Stream 23

* Mon Nov 28 2022 Zhao Zhen <jeremiazhao@tencent.com> - 2.1.0-0.25beta3
- initial
